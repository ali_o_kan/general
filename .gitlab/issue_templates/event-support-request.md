### Issue template for requesting social support for an event
<!-- Are you planning on running an event, and would you like to invite GitLab's community? Please add all the necessary information below, and the Code Contributor Program manager will follow up with you. !-->

### Request

- Event name:
- Date:
- Event description (please keep it short):
- Link to join (for example: zoom link):
- Link to planning issue (if there is one):

### Actions carried out by the submitter:
- Please add a copy to share on social: 
- [ ] [Open an MR](https://about.gitlab.com/events/) for adding the event on GitLab events.


### Actions careed out by a GitLab Team Member
- [ ] @cbacharakis, @jrachel1 or @johncoghlan to add the event on [meetup.com](https://www.meetup.com/gitlab-virtual-meetups/). 
- [ ] Add the event to the [Developer Evangelism calendar](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/#-team-calendar).

### Social Outreach
Anyone, GitLab Team member or community to share a message to:
- Twitter: use your personal account but mention @GitLab
- [Gitter Contributors channel](https://gitter.im/gitlab/contributors).
- [Gitter GitLab channel](https://gitter.im/gitlab/gitlab).
  

### Prior the event (anyone can perform the following actions)
- [ ] Two weeks prior the event, share an invitation on Social
- [ ] One week prior the event, share a reminder on Social
- [ ] One day before the event, share a reminder on Social
- [ ] After the event, share a recap on Social
- [ ] If applicable, share the video recording on [GitLab Unfiltered](https://www.youtube.com/c/GitLabUnfiltered)