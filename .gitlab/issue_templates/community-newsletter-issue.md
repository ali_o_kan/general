
<!-- Use this issue template to create a new issue for the Community Newsletter -->
<!-- Title: Community Newsletter (#[Issue Number]) - Publish Date -->

Submissions to the newsletter are always welcome! [(Handbook section on the newsletter)](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/#community-newsletter)

- You can edit the description to add something to an existing section, or
- Leave a comment with your content if you're unsure and tag @sugaroverflow or @cbacharakis

Thank you! 

Subject: `Community Newsletter - IssueNum - Title`

## Draft

Hey everyone, we hope that you've been having a great summer! Welcome to IssueNumber of the GitLab Newsletter. 

In this issue, we'll talk (something), (something), celebrate the 25th birthday of an open source project, and share our favorite (icebreaker).

Best, 

Christos (@cbacharakis)  & Fatima (@sugaroverflow)
Community Relations, GitLab

### Upcoming

- event highlights, upcoming talks, office hours, or webinars.

### Contributing

- highlighting the MVP
- highlighting a specific MR
- sharing issues ready for contribution 

### Opportunities

- sharing CFPs and open roles 

### Featured

- Featured items include sharing at tip about GitLab or summarizing a blog post, tweet, or recording. 
- This can also be a section to share updates from other newsletters like https://opsindev.news,
- or updates from a specific teammate like a newspaper column.

### Spotlight

- Spotlights include a feature on a GitLab Hero or article they wrote,
- or an Open Source Partner or project.

### Behind the Scenes

- An icebreaker or similar personal share from the Community Relations team.

### Thanks for reading!

- If you enjoyed this newsletter, [forward it to your DevOps friends](https://about.gitlab.com/community/newsletter/).
- Have something to share?[Submissions are always welcome!](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/#community-newsletter)


## Checklist:
- [ ] Add a due date for publishing
- [ ] Add content to sections
- [ ] Ping team members to add or review sections
- [ ] Editorial review
- [ ] When ready to send, Add the `Community Newsletter::Ready` label.
- [ ] After the Newsletter has been sent, replace it with `Community Newsletter::Feedback` and add a week to the due date.

<!-- Please leave the labels below on this issue -->

/label ~"Community Newsletter" ~"Community Newsletter::Draft" ~"dev-evangelism" ~"Community Relations" ~"Code contributor program" ~"DE-Type::Content" ~"DE-Status::ToDo" 
/assign @sugaroverflow @cbacharakis 
/epic https://gitlab.com/groups/gitlab-com/-/epics/1915
