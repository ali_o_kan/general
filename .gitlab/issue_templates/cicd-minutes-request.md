### Issue template for requesting additional CI/CD minutes or a license for contributing to GitLab
This request is of enabling people [contributing to GitLab](https://about.gitlab.com/community/contribute/development/)

### Request
<!-- Please your information below !-->


- GitLab Username:
- Do you need additional CI/CD minutes or a license:
- If you need a lincese, please describe the type (self-hosted, ultimate):
- Repo(s) you have be contributing to:
- Blocked MRs (in case you run out of minutes while opening an MR):

After opening an issue, @cbacharakis will follow up with you (within 24h).
