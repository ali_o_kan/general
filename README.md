# general

Track issues related to the [Code Contributor Program](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/)

Milestones: The names of the milestones are inspired from [the top 100 great paintings](https://en.wikipedia.org/wiki/100_Great_Paintings) list.
